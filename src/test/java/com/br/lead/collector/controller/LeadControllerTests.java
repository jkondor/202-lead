package com.br.lead.collector.controller;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Arrays;
import java.util.Optional;

@WebMvcTest(LeadControler.class)
public class LeadControllerTests {

    @MockBean
    LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    ObjectMapper mapper = new ObjectMapper();
    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("joao@gmail.com");
        lead.setNome("Joao Augusto");

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(15.00);
        produto.setDescricao("melhor café do mundo");
        lead.setProdutos(Arrays.asList(produto));
    }

    @Test
    public void testarCadastroDeLead() throws Exception {

        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        String json = mapper.writeValueAsString(lead);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);


        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.produtos[0].id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarCadastroDeLeadComNomeInvalido() throws Exception {

        lead.setNome("Joao");
        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        String json = mapper.writeValueAsString(lead);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);


        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeDoObjeto", CoreMatchers.equalTo("lead")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.objetoDeErros[0].mensagem", CoreMatchers.equalTo("O nome deve ter entre 8 e 100 caracteres")));
    }

    @Test
    public void testarCadastroDeLeadComEmailInvalido() throws Exception {

        lead.setEmail("lala");
        Iterable<Produto> produtosIterable = Arrays.asList(produto);

        String json = mapper.writeValueAsString(lead);

        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenReturn(lead);
        Mockito.when(leadService.buscarTodosProdutos(Mockito.anyList())).thenReturn(produtosIterable);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isBadRequest())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nomeDoObjeto", CoreMatchers.equalTo("lead")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.objetoDeErros[0].mensagem", CoreMatchers.equalTo("O formato do email é inválido")));
    }

    @Test
    public void testarbuscarTodosLeads() throws Exception {

        Lead lead1 = new Lead();
        lead1.setId(1);
        lead1.setTipoDeLead(TipoDeLead.FRIO);
        lead1.setEmail("jose@gmail.com");
        lead1.setNome("Jose Maria Silva");
        lead1.setProdutos(Arrays.asList(produto));

        Lead lead2 = new Lead();
        lead2.setId(2);
        lead2.setTipoDeLead(TipoDeLead.ORGANICO);
        lead2.setEmail("bruna@gmail.com");
        lead2.setNome("Bruna Maria Silva");
        lead2.setProdutos(Arrays.asList(produto));

        Iterable<Lead> leadIterable = Arrays.asList(lead,lead1,lead2);

        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadIterable);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].id", CoreMatchers.equalTo(0)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].id", CoreMatchers.equalTo(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].nome", CoreMatchers.equalTo("Joao Augusto")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].nome", CoreMatchers.equalTo("Jose Maria Silva")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[2].nome", CoreMatchers.equalTo("Bruna Maria Silva")));
    }

    @Test
    public void testarbuscarNaoEncontrarTodosLeads() throws Exception {

        Iterable<Lead> leadIterable = Arrays.asList();

        Mockito.when(leadService.buscarTodosLeads()).thenReturn(leadIterable);

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$" ).isEmpty());
    }

    @Test
    public void testarBuscarLeadPorId() throws Exception {

        lead.setId(1);
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Joao Augusto")));
    }

    @Test
    public void testarBuscarLeadPorIdNaoExistente() throws Exception {

        lead.setId(1);
        Optional<Lead> leadOptional = Optional.empty();
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/15")
                .contentType(MediaType.APPLICATION_JSON)
                .content(""))
                .andExpect(MockMvcResultMatchers.status().isNoContent())
                .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
    }

    @Test
    public void testarAtualizarLead() throws Exception {

        Lead leadAtualizar = new Lead();
        leadAtualizar.setId(1);
        leadAtualizar.setNome("Jose Maria Silva");

        String json = mapper.writeValueAsString(leadAtualizar);

        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);
        Mockito.when(leadService.atualizarLead(Mockito.any(Lead.class),Mockito.any(Lead.class))).thenReturn(leadAtualizar);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Jose Maria Silva")));
    }


    @Test
    public void testarDeletarLead() throws Exception {
        lead.setId(1);
        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadService.buscarPorId(Mockito.anyInt())).thenReturn(leadOptional);
        Mockito.verify(leadService, Mockito.times(1)).deletarLead(Mockito.any(Lead.class));

        String json = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.delete("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.nome", CoreMatchers.equalTo("Jose Maria Silva")));

    }
}
