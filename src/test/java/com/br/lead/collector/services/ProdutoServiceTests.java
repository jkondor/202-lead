package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTests {
    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void inicializar(){

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(15.00);
        produto.setDescricao("Pilao");
    }

    @Test
    public void testarBuscarTodosProdutos(){

        Iterable<Produto> produtoIterable = Arrays.asList(produto,produto,produto);

        Mockito.when(produtoRepository.findAll()).thenReturn(produtoIterable);
        Iterable<Produto> produtoObjeto = produtoService.buscarTodosProdutos();
        Assertions.assertEquals(produtoObjeto, produtoIterable);
        Mockito.verify(produtoRepository, Mockito.times(1)).findAll();
    }

    @Test
    public void testarBuscarProdutoPorId(){

        Optional<Produto> produtoOptional = Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.any(int.class))).thenReturn(produtoOptional);

        int id=1;
        Optional<Produto> produtoObjeto = produtoService.buscarPorId(id);

        Assertions.assertEquals(produtoObjeto, produtoOptional);
        Mockito.verify(produtoRepository, Mockito.times(1)).findById(Mockito.any(int.class));
    }

    @Test
    public void testarSalvarProduto(){

        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produto);
        Produto proddutoObjeto = produtoService.salvarProduto(produto);
        Assertions.assertEquals(produto, proddutoObjeto);
        Assertions.assertEquals(produto.getId(), proddutoObjeto.getId());
    }

    @Test
    public void testarAtualizarProduto(){

        Produto produtoAlterado = new Produto();
        produtoAlterado.setId(1);
        produtoAlterado.setDescricao("Nestle");


        Mockito.when(produtoRepository.save(Mockito.any(Produto.class))).thenReturn(produtoAlterado);
        Produto produtoObjeto = produtoService.atualizarProduto(produtoAlterado, produto);

        Assertions.assertEquals(produto.getNome(), produtoObjeto.getNome());
        Assertions.assertEquals(produtoAlterado.getNome(), produtoObjeto.getNome());
        Assertions.assertEquals("Nestle", produtoObjeto.getDescricao());
        Assertions.assertEquals(produto.getPreco(), produtoObjeto.getPreco());
        Assertions.assertEquals(produto.getId(), produtoObjeto.getId());
    }

    @Test
    public void testarDeletarProduto(){
        produtoService.deletarProduto(produto);
        Mockito.verify(produtoRepository, Mockito.times(1)).delete(Mockito.any(Produto.class));
    }

}

