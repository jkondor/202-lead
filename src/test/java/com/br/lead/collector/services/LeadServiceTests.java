package com.br.lead.collector.services;

import com.br.lead.collector.enums.TipoDeLead;
import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.*;

@SpringBootTest
public class LeadServiceTests {

    @MockBean
    LeadRepository leadRepository;
    @MockBean
    ProdutoRepository produtoRepository;

    @Autowired
    LeadService leadService;

    Lead lead;
    Produto produto;

    @BeforeEach
    public void inicializar(){
        lead = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.QUENTE);
        lead.setEmail("joao@gmail.com");
        lead.setNome("Joao Augusto");

        produto = new Produto();
        produto.setId(1);
        produto.setNome("Café");
        produto.setPreco(15.00);
        produto.setDescricao("melhor café do mundo");

        lead.setProdutos(Arrays.asList(produto));

    }

    @Test
    public void testarBuscarTodosProdutos(){

        List<Integer> produtosId = new ArrayList<>();
        for (Produto produto: lead.getProdutos() ){
            produtosId.add(produto.getId());
        }
        Iterable<Produto> produtoIterable = Arrays.asList(produto);

        Mockito.when(produtoRepository.findAllById(Mockito.any(List.class))).thenReturn(produtoIterable);

        Iterable<Produto> produtoObjeto = leadService.buscarTodosProdutos(produtosId);

        Assertions.assertEquals(produtoObjeto, produtoIterable);
    }

    @Test
    public void testarBuscarPorId(){

        Optional<Lead> leadOptional = Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.any(int.class))).thenReturn(leadOptional);

        int id=1;
        Optional<Lead> leadObjeto = leadService.buscarPorId(id);

        Assertions.assertEquals(leadObjeto, leadOptional);
        Mockito.verify(leadRepository, Mockito.times(1)).findById(Mockito.any(int.class));
    }

    @Test
    public void testarSalvarLead(){

        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(lead);
        Lead leadObjeto = leadService.salvarLead(lead);
        Assertions.assertEquals(lead, leadObjeto);
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarbuscarTodosLeads(){

        Iterable<Lead> leadIterable = null;
        Mockito.when(leadRepository.findAll()).thenReturn(leadIterable);
        Iterable<Lead> leadObjeto = leadService.buscarTodosLeads();
        Assertions.assertEquals(leadObjeto, leadIterable);
    }

    @Test
    public void testarAtualizarLead(){

        Lead leadData = new Lead();
        lead.setId(1);
        lead.setTipoDeLead(TipoDeLead.FRIO);


        Mockito.when(leadRepository.save(Mockito.any(Lead.class))).thenReturn(leadData);
        Lead leadObjeto = leadService.atualizarLead(leadData, lead);

        Assertions.assertEquals(lead.getTipoDeLead(), leadObjeto.getTipoDeLead());
        Assertions.assertEquals(TipoDeLead.FRIO, leadObjeto.getTipoDeLead());
        Assertions.assertEquals(lead.getNome(), leadObjeto.getNome());
        Assertions.assertEquals(lead.getEmail(), leadObjeto.getEmail());
    }

    @Test
    public void testarDeletarLead(){
        leadService.deletarLead(lead);
        Mockito.verify(leadRepository, Mockito.times(1)).delete(Mockito.any(Lead.class));
    }


}
