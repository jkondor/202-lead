package com.br.lead.collector.controller;


import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.LeadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.util.ArrayList;
import java.util.Optional;
import java.util.List;

@RestController
@RequestMapping("/leads")
public class LeadControler {
    @Autowired
    private LeadService leadService;

    @GetMapping
    public Iterable<Lead> buscarTodosLeads(){
        return leadService.buscarTodosLeads();
    }

    @GetMapping("/{id}")
    public Lead buscarLead(@PathVariable Integer id){

        Optional<Lead> leadOptional = leadService.buscarPorId(id);

        if (leadOptional.isPresent()) {
            return leadOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping
    public ResponseEntity<Lead> salvarLead(@RequestBody @Valid Lead lead){
        List<Integer> produtosId = new ArrayList<>();
        for (Produto produto: lead.getProdutos() ){
            produtosId.add(produto.getId());
        }
        Iterable<Produto> produtoIterable = leadService.buscarTodosProdutos(produtosId);
        lead.setProdutos((List) produtoIterable);

        Lead leadObjeto = leadService.salvarLead(lead);
        return ResponseEntity.status(201).body(leadObjeto);
    }

    @PutMapping("/{id}")
    public Lead atualizarLead(@PathVariable Integer id, @RequestBody Lead lead){
        Lead leadObjeto;
        lead.setId(id);
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()){
            leadObjeto = leadService.atualizarLead(lead, leadOptional.get());
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        return leadObjeto;
    }

    @DeleteMapping("/{id}")
    public Lead deletarLead(@PathVariable int id) {
        Optional<Lead> leadOptional = leadService.buscarPorId(id);
        if (leadOptional.isPresent()) {
            leadService.deletarLead(leadOptional.get());
            return leadOptional.get();
        } else {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }
}
