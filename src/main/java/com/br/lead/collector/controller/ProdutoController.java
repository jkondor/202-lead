package com.br.lead.collector.controller;

import com.br.lead.collector.models.Produto;
import com.br.lead.collector.services.ProdutoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/produtos")
public class ProdutoController {

        @Autowired
        private ProdutoService produtoService;

        @GetMapping
        public Iterable<Produto> buscarTodosProdutos(){
            return produtoService.buscarTodosProdutos();
        }
        @GetMapping("/{id}")
        public Produto buscarProduto(@PathVariable Integer id){
            Optional<Produto> produtoOptional = produtoService.buscarPorId(id);

            if (produtoOptional.isPresent()){
                return produtoOptional.get();
            }else{
                throw new ResponseStatusException(HttpStatus.NO_CONTENT);
            }
        }

        @PostMapping
        public ResponseEntity<Produto> incluirProduto(@RequestBody Produto produto){
            Produto proddutoObjeto = produtoService.salvarProduto(produto);
            return ResponseEntity.status(201).body(proddutoObjeto);
        }

        @PutMapping("/{id}")
        public Produto atualizarProduto(@PathVariable Integer id,@RequestBody Produto produto){
            Produto produtoObjeto;
            produto.setId(id);
            Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
            if (produtoOptional.isPresent()){
                produtoObjeto = produtoService.atualizarProduto(produto, produtoOptional.get());
            }else{
                throw new ResponseStatusException(HttpStatus.NO_CONTENT);
            }

            return produtoObjeto;
        }

        @DeleteMapping("/{id}")
        public Produto deletarProduto(@PathVariable int id) {
            Optional<Produto> produtoOptional = produtoService.buscarPorId(id);
            if (produtoOptional.isPresent()){
                produtoService.deletarProduto(produtoOptional.get());
                return produtoOptional.get();
            }else{
                throw new ResponseStatusException(HttpStatus.NO_CONTENT);
            }
        }

}
