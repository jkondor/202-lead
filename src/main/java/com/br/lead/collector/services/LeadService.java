package com.br.lead.collector.services;

import com.br.lead.collector.models.Lead;
import com.br.lead.collector.models.Produto;
import com.br.lead.collector.repositories.LeadRepository;
import com.br.lead.collector.repositories.ProdutoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Optional;

@Service
public class LeadService {

    @Autowired
    private LeadRepository leadRepository;
    @Autowired
    private ProdutoRepository produtoRepository;

    public Iterable<Produto> buscarTodosProdutos(List<Integer> produtosID){
        Iterable<Produto> produtoIterable = produtoRepository.findAllById(produtosID);
        return  produtoIterable;
    }

    public Optional<Lead> buscarPorId(int id){
        Optional<Lead> leadOptional = leadRepository.findById(id);
        return leadOptional;
    }

    public Lead salvarLead(Lead lead){
        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public Iterable<Lead> buscarTodosLeads() {
        Iterable todosLeads = leadRepository.findAll();
        return todosLeads;
    }

    public Lead atualizarLead(Lead lead, Lead leadData){

        if (lead.getNome()==null){
            lead.setNome(leadData.getNome());
        }
        if (lead.getEmail()==null){
            lead.setEmail(leadData.getEmail());
        }
        if (lead.getTipoDeLead()==null){
            lead.setTipoDeLead(leadData.getTipoDeLead());
        }

        Lead leadObjeto = leadRepository.save(lead);
        return leadObjeto;
    }

    public void deletarLead(Lead lead){
        leadRepository.delete(lead);
    }
}
